FROM mysql

ENV DATA_DIR /data_dump

COPY . /root/code

WORKDIR /root/code

RUN bash -c "chmod a+x run.sh"

CMD ./run.sh -d ${DATA_DIR}