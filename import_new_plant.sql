-----------
-- Replace path of file name and plant id(WERKS) before using this --
-----------

@export on;
@export set filename="/Users/adesai/Desktop/data_dump/KCPlant/MKAL.csv" CsvColumnDelimiter="," ShowNullAs="";
select * from lib_n6p_rtp.sap.MKAL t1 where t1.WERKS='1729';
@export off;

@export on;
@export set filename="/Users/adesai/Desktop/data_dump/KCPlant/CRHD.csv" CsvColumnDelimiter="," ShowNullAs="";
select * from lib_n6p_rtp.sap.CRHD t1 where t1.WERKS='1729';
@export off;

@export on;
@export set filename="/Users/adesai/Desktop/data_dump/KCPlant/PLPO.csv" CsvColumnDelimiter="," ShowNullAs="";
select * from lib_n6p_rtp.sap.PLPO t1 where t1.WERKS='1729';
@export off;

@export on;
@export set filename="/Users/adesai/Desktop/data_dump/KCPlant/PLKO.csv" CsvColumnDelimiter="," ShowNullAs="";
select * from lib_n6p_rtp.sap.PLKO t1 where t1.WERKS='1729';
@export off;

@export on;
@export set filename="/Users/adesai/Desktop/data_dump/KCPlant/MAPL.csv" CsvColumnDelimiter="," ShowNullAs="";
select * from lib_n6p_rtp.sap.MAPL t1 where t1.WERKS='1729';
@export off;

@export on;
@export set filename="/Users/adesai/Desktop/data_dump/KCPlant/MAKT.csv" CsvColumnDelimiter="," ShowNullAs="";
select * from lib_n6p_rtp.sap.MAKT t1 where t1.MATNR in (select distinct t2.MATNR from lib_n6p_rtp.sap.MKAL t2 where t2.WERKS='1729');
@export off;

@export on;
@export set filename="/Users/adesai/Desktop/data_dump/KCPlant/MARA.csv" CsvColumnDelimiter="," ShowNullAs="";
select * from lib_n6p_rtp.sap.MARA t1 where t1.MATNR in (select distinct t2.MATNR from lib_n6p_rtp.sap.MKAL t2 where t2.WERKS='1729');
@export off;

@export on;
@export set filename="/Users/adesai/Desktop/data_dump/KCPlant/MARC.csv" CsvColumnDelimiter="," ShowNullAs="";
select * from lib_n6p_rtp.sap.MARC t1 where t1.WERKS='1729';
@export off;

@export on;
@export set filename="/Users/adesai/Desktop/data_dump/KCPlant/SKU_STATUS.csv" CsvColumnDelimiter="," ShowNullAs="";
select distinct t1.MATNR AS SKU_ID, IS_ACTIVE=0 from lib_n6p_rtp.sap.MARC t1 where t1.WERKS='1729';
@export off;

@export on;
@export set filename="/Users/adesai/Desktop/data_dump/KCPlant/PLAS.csv" CsvColumnDelimiter="," ShowNullAs="";
select T1.*
from sap.PLAS T1
LEFT JOIN sap.MKAL T2 ON T1.PLNTY=T2.PLNTY AND T1.MANDT=T2.MANDT AND T1.PLNNR=T2.PLNNR AND T1.PLNAL=T2.ALNAL
where T2.WERKS='1729';
@export off;

@export on;
@export set filename="/Users/adesai/Desktop/data_dump/KCPlant/ZTXXPT010X.csv" CsvColumnDelimiter="," ShowNullAs="";
SELECT
	Z1.MATNR AS MATERIAL_ID,
	Z1.ZATTRTYPID AS ATTRIBUTE_TYPE_ID,
	Z1.ZATTRVALID AS ATTRIBUTE_VALUE_ID,
	Z2.ZATTRVALDC AS ATTRIBUTE_VALUE_DESCRIPTION
FROM
	sap.ZTXXPT0104 Z1 JOIN sap.ZTXXPT0103 Z2 ON Z1.ZATTRTYPID=Z2.ZATTRTYPID AND Z1.ZATTRVALID=Z2.ZATTRVALID AND Z1.MANDT=Z2.MANDT
WHERE
	Z1.ZATTRTYPID IN ('SECTOR','SUB_SECTOR','CATEGORY', 'SEGMENT', 'BRAND')
	AND Z1.MATNR in (
		SELECT X1.MATNR
		FROM sap.MARC AS X1 JOIN sap.MARA AS X2 ON X1.MATNR=X2.MATNR AND X1.MATNR=X2.MATNR
		WHERE X1.WERKS='1729' AND X2.LVORM !='X' AND X2.MTART IN ('FERT'))
ORDER BY Z1.MATNR, Z1.ZATTRTYPID;
@export off;



