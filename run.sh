#!/bin/bash

curDir=$PWD/schema

usage="Usage: sh run.sh [-d|--dir] <home-directory>"

dir="/data_dump"
DB_NAME="XPLANNER_MASTER_DATA"

while [ "$1" != "" ]; do
    case $1 in
        -d | --dir )           shift
                                dir=$1
                                ;;
        * )                     echo $usage
                                exit 1
    esac
    shift
done

files="$(find "$dir" -name "*.csv")"

echo "Data directory is $dir"
echo "Creating sap tables"

for table in $curDir/sap/*.sql; do
	if mysql -u root $DB_NAME -h $DB_HOST < "$table"; then
	  echo "Created: $table"
	else
	  echo "Error creating table: $table"
	fi
done

echo "Injecting data in sap tables"
for f in $files
do
  echo $f
  f1=`basename $f`
	tableName=`echo $f1|cut -d . -f 1`
	if mysql -u root $DB_NAME -h $DB_HOST -e "
      LOAD DATA LOCAL INFILE '$f'
      INTO TABLE $tableName
      FIELDS TERMINATED BY ','
      LINES TERMINATED BY '\n'
      IGNORE 1 LINES;"; then
    echo "Successfully injected in table:$tableName"
  else
    echo "Error injecting in table:$tableName"
  fi
done

echo "Creating model helper tables"
for table in $curDir/model_helper/*.sql; do
 	if mysql -u root $DB_NAME -h $DB_HOST < "$table"; then
 	   echo "Created: $table"
	else
	  echo "Error creating table: $table"
	fi
done

echo "Creating model tables"
for table in $curDir/model/*.sql; do
 	if mysql -u root $DB_NAME -h $DB_HOST < "$table"; then
 	   echo "Created: $table"
	else
	  echo "Error creating table: $table"
	fi
done

arr=("LINE" "SKU")
for t in ${arr[@]}; do
    if mysql -u root $DB_NAME -h $DB_HOST -e "update $t set LAST_MODIFIED_AT=NOW()"; then
        echo "updated last modified field successfully: $t"
    else echo "Failed to update last modified field: $t"
    fi
done