DROP TABLE IF EXISTS MKAL;
CREATE TABLE MKAL (
	MANDT nvarchar(3) NOT NULL,
	MATNR nvarchar(18) NOT NULL,
	WERKS nvarchar(4) NOT NULL,
	VERID nvarchar(4) NOT NULL,
	BDATU nvarchar(8),
	ADATU nvarchar(8),
	STLAL nvarchar(2),
	STLAN nvarchar(1),
	PLNTY nvarchar(1),
	PLNNR nvarchar(8),
	ALNAL nvarchar(2),
	BESKZ nvarchar(1),
	SOBSL nvarchar(2),
	LOSGR decimal(13,3),
	MDV01 nvarchar(8),
	MDV02 nvarchar(8),
	TEXT1 nvarchar(40),
	EWAHR decimal(3),
	VERTO nvarchar(4),
	SERKZ nvarchar(1),
	BSTMI decimal(13,3),
	BSTMA decimal(13,3),
	RGEKZ nvarchar(1),
	ALORT nvarchar(4),
	PLTYG nvarchar(1),
	PLNNG nvarchar(8),
	ALNAG nvarchar(2),
	PLTYM nvarchar(1),
	PLNNM nvarchar(8),
	ALNAM nvarchar(2),
	CSPLT nvarchar(4),
	MATKO nvarchar(18),
	ELPRO nvarchar(4),
	PRVBE nvarchar(10),
	PRFG_F nvarchar(1),
	PRDAT nvarchar(8),
	MKSP nvarchar(1),
	PRFG_R nvarchar(1),
	PRFG_G nvarchar(1),
	PRFG_S nvarchar(1),
	UCMAT nvarchar(18),
	PPEGUID varbinary(18),
	BDATU_SIMP_DT date,
	ADATU_SIMP_DT date,
	PRDAT_SIMP_DT date,
	PRIMARY KEY (MANDT,MATNR,VERID,WERKS)
);