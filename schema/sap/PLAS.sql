DROP TABLE IF EXISTS PLAS;
CREATE TABLE PLAS (
  MANDT nvarchar(3),
  PLNTY nvarchar(1),
  PLNNR nvarchar(8),
  PLNAL nvarchar(2),
  PLNFL nvarchar(6),
  PLNKN nvarchar(8),
  ZAEHL nvarchar(8),
  DATUV nvarchar(8),
  TECHV nvarchar(12),
  AENNR nvarchar(12),
  LOEKZ nvarchar(1),
  PARKZ nvarchar(1),
  ANDAT nvarchar(8),
  ANNAM nvarchar(12),
  AEDAT nvarchar(8),
  AENAM nvarchar(12),
  DATUV_SIMP_DT date,
  ANDAT_SIMP_DT date,
  AEDAT_SIMP_DT date,
  PRIMARY KEY (MANDT,PLNTY,PLNNR,PLNAL,PLNFL,PLNKN,ZAEHL)
);