#!/bin/bash
set -xe

source scripts/common.sh

echo "Booting valuestream-onboarding-data-ingester service"

docker-compose -p valuestream-onborading -f docker-compose.yml up --remove-orphans data-ingester
