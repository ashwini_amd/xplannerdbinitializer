#!/bin/bash
set -xe

source scripts/common.sh

echo "Building valuestream-onboarding-data-ingester service"
docker-compose -p valuestream-onborading -f docker-compose.yml build data-ingester
