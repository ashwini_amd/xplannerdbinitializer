#!/bin/bash
set -xe

export ENV=${ENV:-dev}
export DOCKER_HOST_IP=${DOCKER_HOST_IP:-192.168.99.100} #ip of mysql server container
export SERVICE_PORT=${SERVICE_PORT:-3306} #port of mysql server container
export DATA_DIR=${DATA_DIR:-/var/data_dump} #port of mysql server container