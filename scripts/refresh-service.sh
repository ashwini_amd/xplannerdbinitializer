#!/usr/bin/env bash

set -xe

echo "Refreshing valuestream-onboarding-data-ingester service"

chmod a+x scripts/*.sh

scripts/stop-service.sh
scripts/build-image.sh
scripts/boot-service.sh

echo 'Done refreshing services'
