#!/bin/bash
source scripts/common.sh

echo "Stopping valuestream-onboarding-data-ingester service"

stopContainerWith() {
    criteria=$1
    container_id=`docker ps -a -q -f $criteria`
    if [ -n "$container_id" ]; then
      docker stop $container_id
      docker rm $container_id
    fi
}

stopContainerWith "NAME=$ENV-valuestream-onboarding-data-ingester"

exit 0
